# Magic Carpet Plugin
The plugin generates a flying magic carpet. Your carpet has a mileage recorder (how many blocks have you already traveled). In the configuration file you can set the name of the carpet, size and color.

## Contributing
If you wish to contribute it with me, just send the join request.
