package com.gmail.kolecka96.spigot.magiccarpetplugin.listener;

import com.gmail.kolecka96.spigot.magiccarpetplugin.MagicCarpetPlugin;
import com.gmail.kolecka96.spigot.magiccarpetplugin.converter.MessageConverter;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.Carpet;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.OwnerCarpetDetail;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class MagicCarpetListenerTest {
	@Test
	void succeededThrowMagicCarpet() {
		// given
		final Player player = mock(Player.class);
		final ItemStack itemStack = new ItemStack(Material.PINK_CARPET);
		final MagicCarpetPlugin magicCarpetPlugin = mock(MagicCarpetPlugin.class);
		final MessageConverter messageConverter = mock(MessageConverter.class);
		final MagicCarpetListener magicCarpetListener = new MagicCarpetListener(magicCarpetPlugin, messageConverter);
		final BlockPlaceEvent blockPlaceEvent = mock(BlockPlaceEvent.class);
		final Block block = mock(Block.class);

		when(blockPlaceEvent.getItemInHand()).thenReturn(itemStack);
		when(magicCarpetPlugin.getItemStack()).thenReturn(itemStack);
		when(blockPlaceEvent.getBlock()).thenReturn(block);
		when(blockPlaceEvent.getPlayer()).thenReturn(player);
		when(magicCarpetPlugin.getCarpet()).thenReturn(new Carpet("TEST", Material.PINK_CARPET, 10,10,"pink"));

		// when
		magicCarpetListener.throwMagicCarpet(blockPlaceEvent);

		// then
		verify(magicCarpetPlugin, times(1)).addCarpetUser(new OwnerCarpetDetail(player));
	}

	@Test
	void failedThrowMagicCarpet() {
		// given
		final Player player = mock(Player.class);
		final MagicCarpetPlugin magicCarpetPlugin = mock(MagicCarpetPlugin.class);
		final MessageConverter messageConverter = mock(MessageConverter.class);
		final MagicCarpetListener magicCarpetListener = new MagicCarpetListener(magicCarpetPlugin, messageConverter);
		final BlockPlaceEvent blockPlaceEvent = mock(BlockPlaceEvent.class);

		when(blockPlaceEvent.getItemInHand()).thenReturn(new ItemStack(Material.PINK_CARPET));
		when(magicCarpetPlugin.getItemStack()).thenReturn(new ItemStack(Material.PINK_BED));

		// when
		magicCarpetListener.throwMagicCarpet(blockPlaceEvent);

		// then
		verify(magicCarpetPlugin, times(0)).addCarpetUser(new OwnerCarpetDetail(player));
	}

	@Test
	void succeededStepOnCarpet() {
		//given
		final Player player = mock(Player.class);
		final World world = mock(World.class);
		final Block block = mock(Block.class);
		final MagicCarpetPlugin magicCarpetPlugin = mock(MagicCarpetPlugin.class);
		final MessageConverter messageConverter = mock(MessageConverter.class);
		final MagicCarpetListener magicCarpetListener = new MagicCarpetListener(magicCarpetPlugin, messageConverter);
		final PlayerMoveEvent playerMoveEvent = mock(PlayerMoveEvent.class);
		final Location location = new Location(world, 10, 10, 10);

		when(playerMoveEvent.getPlayer()).thenReturn(player);
		when(block.getLocation()).thenReturn(location);
		when(playerMoveEvent.getFrom()).thenReturn(location);
		when(magicCarpetPlugin.getCarpet()).thenReturn(new Carpet("TEST", Material.PINK_CARPET, 10,10,"pink"));
		when(playerMoveEvent.getTo()).thenReturn(location);
		when(location.getBlock()).thenReturn(block);
		when(magicCarpetPlugin.isInvokedCommandOn(player)).thenReturn(true);
		when(magicCarpetPlugin.getOwnerCarpetDetailsByPlayer(playerMoveEvent.getPlayer())).thenReturn(new OwnerCarpetDetail(player));

		//when
		magicCarpetListener.stepOnCarpet(playerMoveEvent);

		//then
		verify(magicCarpetPlugin, times(1)).getMileageCounter();
	}

	@Test
	void failedStepOnCarpet() {
		//given
		final Player player = mock(Player.class);
		final MagicCarpetPlugin magicCarpetPlugin = mock(MagicCarpetPlugin.class);
		final MessageConverter messageConverter = mock(MessageConverter.class);
		final MagicCarpetListener magicCarpetListener = new MagicCarpetListener(magicCarpetPlugin, messageConverter);
		final PlayerMoveEvent playerMoveEvent = mock(PlayerMoveEvent.class);

		when(magicCarpetPlugin.isInvokedCommandOn(player)).thenReturn(false);

		//when
		magicCarpetListener.stepOnCarpet(playerMoveEvent);

		//then
		verify(magicCarpetPlugin, times(0)).getMileageCounter();
	}

	@Test
	void failedStepOnCarpetWhenOwnerCarpetDetailsIsNull() {
		//given
		final Player player = mock(Player.class);
		final MagicCarpetPlugin magicCarpetPlugin = mock(MagicCarpetPlugin.class);
		final MessageConverter messageConverter = mock(MessageConverter.class);
		final MagicCarpetListener magicCarpetListener = new MagicCarpetListener(magicCarpetPlugin, messageConverter);
		final PlayerMoveEvent playerMoveEvent = mock(PlayerMoveEvent.class);

		when(magicCarpetPlugin.isInvokedCommandOn(player)).thenReturn(true);
		when(magicCarpetPlugin.getOwnerCarpetDetailsByPlayer(playerMoveEvent.getPlayer())).thenReturn(null);

		//when
		magicCarpetListener.stepOnCarpet(playerMoveEvent);

		//then
		verify(magicCarpetPlugin, times(0)).getMileageCounter();
	}
}
