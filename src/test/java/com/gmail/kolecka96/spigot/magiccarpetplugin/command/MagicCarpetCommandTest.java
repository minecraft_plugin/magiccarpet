package com.gmail.kolecka96.spigot.magiccarpetplugin.command;

import com.gmail.kolecka96.spigot.magiccarpetplugin.MagicCarpetPlugin;
import com.gmail.kolecka96.spigot.magiccarpetplugin.converter.MessageConverter;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.Carpet;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.MileageCounter;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.OwnerCarpetDetail;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.ScoreboardManager;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class MagicCarpetCommandTest {
	@Test
	void failedCarpetOff() {
		// given
		final MagicCarpetPlugin magicCarpetPlugin = mock(MagicCarpetPlugin.class);
		final Player sender = mock(Player.class);
		final MessageConverter messageConverter = mock(MessageConverter.class);
		final MagicCarpetCommand magicCarpetCommand = new MagicCarpetCommand(sender, magicCarpetPlugin,  messageConverter);

		when(magicCarpetPlugin.getCarpet()).thenReturn(new Carpet("test", Material.PINK_CARPET, 10, 10, "pink"));
		when(messageConverter.formatMessage("messages.information.carpet-off-warning"))
				.thenReturn("&2Oops! You dont have your private carpet, so you cannot off it!");

		// when
		String result = magicCarpetCommand.off();

		// then
		assertEquals("&2Oops! You dont have your private carpet, so you cannot off it!", result);
	}

	@Test
	void succeededCarpetOff() {
		// given
		final MagicCarpetPlugin magicCarpetPlugin = mock(MagicCarpetPlugin.class);
		final Player sender = mock(Player.class);
		final Server server = mock(Server.class);
		final ScoreboardManager scoreboardManager = mock(ScoreboardManager.class);
		final OwnerCarpetDetail ownerCarpetDetail = new OwnerCarpetDetail(sender);
		ownerCarpetDetail.setLastMileageCounter(10);
		final MessageConverter messageConverter = mock(MessageConverter.class);
		final MagicCarpetCommand magicCarpetCommand = new MagicCarpetCommand(sender, magicCarpetPlugin,  messageConverter);

		when(magicCarpetPlugin.getCarpet()).thenReturn(new Carpet("test", Material.PINK_CARPET, 10, 10, "pink"));
		when(magicCarpetPlugin.getOwnerCarpetDetailsByPlayer(sender)).thenReturn(ownerCarpetDetail);
		when(magicCarpetPlugin.getMileageCounter()).thenReturn(new MileageCounter(magicCarpetPlugin, messageConverter));
		when(magicCarpetPlugin.getServer()).thenReturn(server);
		when(server.getScoreboardManager()).thenReturn(scoreboardManager);
		when(messageConverter.formatMessage("messages.information.carpet-mileage-counter", Map.of("{CURRENT_MILEAGE}", "10")))
				.thenReturn("&2You parked up your carpet. You flew: &610 blocks/game");

		// when
		final String result = magicCarpetCommand.off();

		// then
		assertEquals("&2You parked up your carpet. You flew: &610 blocks/game", result);
	}
}
