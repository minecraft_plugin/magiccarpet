package com.gmail.kolecka96.spigot.magiccarpetplugin;

import com.gmail.kolecka96.spigot.magiccarpetplugin.command.MagicCarpetCommand;
import com.gmail.kolecka96.spigot.magiccarpetplugin.converter.MessageConverter;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.Carpet;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.MileageCounter;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.OwnerCarpetDetail;
import com.gmail.kolecka96.spigot.magiccarpetplugin.exception.CarpetException;
import com.gmail.kolecka96.spigot.magiccarpetplugin.listener.MagicCarpetListener;
import com.gmail.kolecka96.spigot.magiccarpetplugin.utils.Constant;
import eu.andret.arguments.AnnotatedCommand;
import eu.andret.arguments.CommandManager;
import lombok.Getter;
import org.bstats.bukkit.Metrics;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Stream;

@Getter
public final class MagicCarpetPlugin extends JavaPlugin {
	private Carpet carpet;
	private ItemStack itemStack;
	private final MessageConverter messageConverter = new MessageConverter(this);
	private final MileageCounter mileageCounter = new MileageCounter(this, messageConverter);
	private final List<OwnerCarpetDetail> players = new ArrayList<>();
	private final File messagesFile = new File(getDataFolder(), "messages.yml");
	private YamlConfiguration ymlConfiguration;
	@Nullable
	private Connection connection;

	@Override
	public void onEnable() {
		if (!messagesFile.exists()) {
			saveResource("messages.yml", false);
		}
		ymlConfiguration = YamlConfiguration.loadConfiguration(messagesFile);
		new Metrics(this, 14619);
		saveDefaultConfig();
		setupDatabase();
		carpet = createCarpetFromConfig();
		itemStack = toSpecificCarpet();
		getServer().addRecipe(generateRecipe());
		AnnotatedCommand<MagicCarpetPlugin> command = CommandManager.registerCommand(MagicCarpetCommand.class, this, messageConverter);
		command.setOnInsufficientPermissionsListener(sender -> sender.sendMessage("You don't have permissions"));
		command.setOnUnknownSubCommandExecutionListener(sender -> sender.sendMessage("I don't know what you want from me"));
		getServer().getPluginManager().registerEvents(new MagicCarpetListener(this, messageConverter), this);
	}

	public void addCarpetUser(@NotNull final OwnerCarpetDetail player) {
		players.add(player);
	}

	public void removeCommandOnPlayer(@NotNull final Player player) {
		players.stream()
				.filter(ownerCarpetDetail -> ownerCarpetDetail.getPlayer().equals(player))
				.findFirst()
				.ifPresent(players::remove);
	}

	public boolean isInvokedCommandOn(@NotNull final Player player) {
		return players.stream()
				.map(OwnerCarpetDetail::getPlayer)
				.anyMatch(player::equals);
	}

	public OwnerCarpetDetail getOwnerCarpetDetailsByPlayer(@NotNull final Player player) {
		return players.stream()
				.filter(ownerCarpetDetail -> ownerCarpetDetail.getPlayer().equals(player))
				.findFirst()
				.orElse(null);
	}

	public YamlConfiguration getMessagesConfig() {
		return ymlConfiguration;
	}

	public void show(@NotNull final Location location, @NotNull final Material material, @NotNull final UUID playerId) {
		Stream.iterate(getMinValue(carpet.getWidth()), n -> n + 1)
				.limit(carpet.getWidth())
				.forEach(z ->
						Stream.iterate(getMinValue(carpet.getLength()), m -> m + 1)
								.limit(carpet.getLength())
								.forEach(x -> {
											final Block block = location.clone().add(x, -1, z).getBlock();
											if (block.getType() == Material.AIR) {
												block.setType(material);
												block.setMetadata(Constant.TYPE_METADATA, new FixedMetadataValue(this, playerId));
											}
										}
								));
	}

	public void hide(@NotNull final Location location, @NotNull final Material material) {
		Stream.iterate(getMinValue(carpet.getWidth()), z -> z + 1)
				.limit(carpet.getWidth())
				.forEach(z ->
						Stream.iterate(getMinValue(carpet.getLength()), x -> x + 1)
								.limit(carpet.getLength())
								.forEach(x -> {
											final Block block = location.clone().add(x, -1, z).getBlock();
											if (block.getType() == material) {
												block.setType(Material.AIR);
											}
										}
								));
	}

	private Carpet createCarpetFromConfig() {
		return Optional.of(getConfig())
				.map(config -> config.getConfigurationSection("carpet"))
				.map(configurationSection -> {
					final String name = configurationSection.getString("name");
					if (name == null) {
						throw new CarpetException("Carpet name cannot be null or empty!");
					}
					final int selectedLength = configurationSection.getInt("length");
					if (selectedLength <= 0) {
						throw new CarpetException("Length cannot be zero or empty!");
					}
					final int selectedWidth = configurationSection.getInt("width");
					if (selectedWidth <= 0) {
						throw new CarpetException("Width cannot be zero or empty!");
					}
					final String color = configurationSection.getString("color");
					if (color == null) {
						throw new CarpetException("Color cannot be null or empty!");
					}
					final Material material = toCarpetMaterial(color,"_WOOL");
					return new Carpet(name, material, selectedWidth, selectedLength, color);
				})
				.orElse(null);
	}

	private int getMinValue(final int value) {
		return -value / 2;
	}

	@NotNull
	private Material toCarpetMaterial(@NotNull final String color, @NotNull final String type) {
		final Material carpetMaterial = Material.getMaterial(color.concat(type));
		if (carpetMaterial == null) {
			throw new CarpetException("Selected color is invalid!");
		}
		return carpetMaterial;
	}

	@NotNull
	private ShapedRecipe generateRecipe() {
		final ShapedRecipe shape = new ShapedRecipe(new NamespacedKey(this, "magic_carpet"), itemStack);
		shape.shape(" S ", " L ", " S ");
		shape.setIngredient('L', Material.LIGHT_WEIGHTED_PRESSURE_PLATE);
		shape.setIngredient('S', Material.SUNFLOWER);
		return shape;
	}

	@NotNull
	private ItemStack toSpecificCarpet() {
		final ItemStack itemStack = new ItemStack(toCarpetMaterial(carpet.getColor(), "_CARPET"));
		final ItemMeta itemMeta = itemStack.getItemMeta();
		Optional.ofNullable(itemMeta)
				.ifPresent(itemMetaElement -> {
					itemMetaElement.setDisplayName(carpet.getName());
					itemStack.setItemMeta(itemMetaElement);
				});
		return itemStack;
	}

	private void setupDatabase() {
		final boolean databaseEnabled = getConfig().getBoolean("database.enabled", false);
		if (!databaseEnabled) {
			getLogger().warning("Database is disabled. In order to save records, enable it in config.");
			return;
		}
		try {
			connection = createConnection();
			initDatabase();
			getLogger().info("Database connection established.");
		} catch (final SQLException ex) {
			getLogger().severe("An error occurred when trying to connect to database.");
			connection = null;
			ex.printStackTrace();
		}
	}

	@NotNull
	private Connection createConnection() throws SQLException {
		final String url = getConfig().getString("database.url", "localhost");
		final String user = getConfig().getString("database.user", "root");
		final String pass = getConfig().getString("database.pass", "");
		final String database = getConfig().getString("database.dbname", "magic_carpet");
		return DriverManager.getConnection(String.format("jdbc:mysql://%s/%s?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false", url, database), user, pass);
	}

	private void initDatabase() {
		getConnection().ifPresent(conn -> {
			try (final InputStream inputStream = getClassLoader().getResourceAsStream("setup.sql")) {
				if (inputStream == null) {
					return;
				}
				Optional.ofNullable(inputStream.readAllBytes())
						.map(String::new)
						.map(content -> content.split(";"))
						.stream()
						.flatMap(Arrays::stream)
						.filter(query -> !query.isBlank())
						.forEach(query -> {
							try (final PreparedStatement statement = conn.prepareStatement(query)) {
								statement.execute();
							} catch (final SQLException e) {
								getLogger().log(Level.SEVERE, "An error occurred when trying to execute query", e);
							}
						});
				getLogger().info("§2Database setup complete.");
			} catch (final IOException e) {
				getLogger().log(Level.SEVERE, "Cannot execute SQL statement", e);
			}
		});
	}

	@NotNull
	public Optional<Connection> getConnection() {
		return Optional.ofNullable(connection);
	}
}
