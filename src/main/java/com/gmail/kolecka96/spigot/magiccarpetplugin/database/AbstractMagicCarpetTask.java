package com.gmail.kolecka96.spigot.magiccarpetplugin.database;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

@AllArgsConstructor
public abstract class AbstractMagicCarpetTask implements Runnable {
	@NotNull
	protected final Connection connection;
}
