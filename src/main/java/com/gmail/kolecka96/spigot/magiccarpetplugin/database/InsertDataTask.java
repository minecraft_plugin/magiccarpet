package com.gmail.kolecka96.spigot.magiccarpetplugin.database;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

@Value
@EqualsAndHashCode(callSuper = true)
public class InsertDataTask extends AbstractMagicCarpetTask {
	@NotNull
	UUID playerId;
	int mileage;
	Consumer<CarpetTaskResult> consumer;

	public InsertDataTask(@NotNull final Connection connection, @NotNull final UUID playerId, final int mileage, @Nullable final Consumer<CarpetTaskResult> consumer) {
		super(connection);
		this.playerId = playerId;
		this.mileage = mileage;
		this.consumer = consumer;
	}

	@Override
	public void run() {
		try (final PreparedStatement stat = connection.prepareStatement("INSERT INTO mileage VALUES(NULL, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			final LocalDateTime now = LocalDateTime.now();
			stat.setString(1, playerId.toString());
			stat.setInt(2, mileage);
			stat.setTimestamp(3, Timestamp.valueOf(now));
			stat.execute();
			final ResultSet generatedKeys = stat.getGeneratedKeys();
			if (generatedKeys.next()) {
				final int id = generatedKeys.getInt(1);
				Optional.ofNullable(consumer).ifPresent(c -> c.accept(new CarpetTaskResult(id, playerId, mileage, now)));
			}
		} catch (final SQLException ex) {
			Optional.ofNullable(consumer).ifPresent(c -> c.accept(null));
			ex.printStackTrace();
		}
	}
}
