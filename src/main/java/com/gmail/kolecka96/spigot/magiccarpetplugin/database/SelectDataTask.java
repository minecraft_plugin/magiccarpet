package com.gmail.kolecka96.spigot.magiccarpetplugin.database;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

@EqualsAndHashCode(callSuper = true)
@Value
public class SelectDataTask extends AbstractMagicCarpetTask {
	@NotNull
	UUID playerId;
	Consumer<Integer> callback;

	public SelectDataTask(@NotNull Connection connection, @NotNull UUID playerId, Consumer<Integer> callback) {
		super(connection);
		this.playerId = playerId;
		this.callback = callback;
	}

	@Override
	public void run() {
		try (final PreparedStatement stat = connection.prepareStatement("SELECT sum(counter) AS counter FROM mileage WHERE user_id = ?")) {
			stat.setString(1, playerId.toString());
			final ResultSet resultSet = stat.executeQuery();
			if (!resultSet.next()) {
				Optional.ofNullable(callback).ifPresent(callBack -> callBack.accept(null));
			}
			final int result = resultSet.getInt("counter");
			Optional.ofNullable(callback).ifPresent(callBack -> callBack.accept(result));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
