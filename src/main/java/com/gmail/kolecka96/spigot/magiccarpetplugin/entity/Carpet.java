package com.gmail.kolecka96.spigot.magiccarpetplugin.entity;

import lombok.Value;
import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

@Value
public class Carpet {
	@NotNull
	String name;
	@NotNull
	Material material;
	int width;
	int length;
	@NotNull
	String color;
}
