package com.gmail.kolecka96.spigot.magiccarpetplugin.command;

import com.gmail.kolecka96.spigot.magiccarpetplugin.MagicCarpetPlugin;
import com.gmail.kolecka96.spigot.magiccarpetplugin.converter.MessageConverter;
import com.gmail.kolecka96.spigot.magiccarpetplugin.database.CarpetTaskResult;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.OwnerCarpetDetail;
import com.gmail.kolecka96.spigot.magiccarpetplugin.database.InsertDataTask;
import eu.andret.arguments.AnnotatedCommandExecutor;
import eu.andret.arguments.api.annotation.Argument;
import eu.andret.arguments.api.annotation.BaseCommand;
import eu.andret.arguments.api.entity.ExecutorType;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.function.Consumer;

@BaseCommand("carpet")
public class MagicCarpetCommand extends AnnotatedCommandExecutor<MagicCarpetPlugin> {
	private final MessageConverter converter;

	public MagicCarpetCommand(final CommandSender sender, final MagicCarpetPlugin plugin, final MessageConverter converter) {
		super(sender, plugin);
		this.converter = converter;
	}

	@Argument(permission = "magiccarpet.off", executorType = ExecutorType.PLAYER, description = "Carpet, stop!")
	public String off() {
		final Player player = (Player) sender;
		plugin.hide(player.getLocation(), plugin.getCarpet().getMaterial());
		final OwnerCarpetDetail ownerCarpetDetail = plugin.getOwnerCarpetDetailsByPlayer(player);
		if (ownerCarpetDetail == null) {
			return converter.formatMessage("messages.information.carpet-off-warning");
		}
		final int lastMileageCounter = ownerCarpetDetail.getLastMileageCounter();
		plugin.removeCommandOnPlayer(player);
		performDatabaseOperations(player, lastMileageCounter, carpetTaskResult -> {
			if (carpetTaskResult == null) {
				System.out.println("##Failed process! New record cannot be insert into db!");
			}
		});
		plugin.getMileageCounter().hideMileageCounter(player);
		return converter.formatMessage("messages.information.carpet-mileage-counter", Map.of("{CURRENT_MILEAGE}", String.valueOf(lastMileageCounter)));
	}

	private void performDatabaseOperations(@NotNull final Player player, final int mileage, @Nullable final Consumer<CarpetTaskResult> consumer) {
		plugin.getConnection()
				.map(connection -> new InsertDataTask(connection, player.getUniqueId(), mileage, consumer))
				.ifPresent(insertDataTask -> plugin.getServer().getScheduler().runTaskAsynchronously(plugin, insertDataTask));
	}
}
