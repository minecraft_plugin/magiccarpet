package com.gmail.kolecka96.spigot.magiccarpetplugin.exception;

import lombok.Getter;

@Getter
public class CarpetException extends RuntimeException {
	public CarpetException(final String message) {
		super(message);
	}
}
