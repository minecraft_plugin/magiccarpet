package com.gmail.kolecka96.spigot.magiccarpetplugin.converter;

import com.gmail.kolecka96.spigot.magiccarpetplugin.MagicCarpetPlugin;
import com.gmail.kolecka96.spigot.magiccarpetplugin.exception.CarpetException;
import lombok.Value;
import org.bukkit.ChatColor;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

@Value
public class MessageConverter {
	MagicCarpetPlugin magicCarpetPlugin;

	@NotNull
	public String formatMessage(final String path) {
		final String message = getMessage(path);
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	@NotNull
	public String formatMessage(final String path, final Map<String, String> input) {
		String message = getMessage(path);
		for (Map.Entry<String, String> entry : input.entrySet()) {
			message = message.replace(entry.getKey(), entry.getValue());
		}
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	@NotNull
	private String getMessage(final String path) {
		final String message = magicCarpetPlugin.getMessagesConfig().getString(path);
		if (message == null) {
			throw new CarpetException("This message doesn't exist!");
		}
		return message;
	}
}
