package com.gmail.kolecka96.spigot.magiccarpetplugin.database;

import lombok.Value;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.util.UUID;

@Value
public class CarpetTaskResult {
	int id;
	@NotNull
	UUID playerId;
	int mileage;
	@NotNull
	LocalDateTime time;
}
