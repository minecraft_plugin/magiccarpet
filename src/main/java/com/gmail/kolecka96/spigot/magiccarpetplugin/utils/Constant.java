package com.gmail.kolecka96.spigot.magiccarpetplugin.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constant {
	public static final String TYPE_METADATA = "type";
}
