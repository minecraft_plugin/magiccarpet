package com.gmail.kolecka96.spigot.magiccarpetplugin.listener;

import com.gmail.kolecka96.spigot.magiccarpetplugin.MagicCarpetPlugin;
import com.gmail.kolecka96.spigot.magiccarpetplugin.converter.MessageConverter;
import com.gmail.kolecka96.spigot.magiccarpetplugin.database.SelectDataTask;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.OwnerCarpetDetail;
import com.gmail.kolecka96.spigot.magiccarpetplugin.entity.MileageCounter;
import com.gmail.kolecka96.spigot.magiccarpetplugin.utils.Constant;
import lombok.AllArgsConstructor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.metadata.MetadataValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Map;

@AllArgsConstructor
public class MagicCarpetListener implements Listener {
	private final MagicCarpetPlugin plugin;
	private final MessageConverter converter;

	@EventHandler
	public void stepOnCarpet(final PlayerMoveEvent event) {
		if (!plugin.isInvokedCommandOn(event.getPlayer())) {
			return;
		}
		final OwnerCarpetDetail ownerCarpetDetail = plugin.getOwnerCarpetDetailsByPlayer(event.getPlayer());
		if (ownerCarpetDetail == null) {
			return;
		}
		showCarpetMileageCounter(event, ownerCarpetDetail);
		final Material carpet = plugin.getCarpet().getMaterial();
		plugin.hide(event.getFrom(), carpet);
		if (event.getPlayer().isSneaking()) {
			plugin.show(Objects.requireNonNull(event.getTo()).clone().add(0, -1, 0), carpet, ownerCarpetDetail.getPlayer().getUniqueId());
		} else {
			plugin.show(event.getTo(), carpet, ownerCarpetDetail.getPlayer().getUniqueId());
		}
	}

	@EventHandler
	public void throwMagicCarpet(final BlockPlaceEvent event) {
		if (Objects.equals(event.getItemInHand(), plugin.getItemStack())) {
			final Player player = event.getPlayer();
			event.getBlock().setType(Material.AIR);
			player.sendMessage(converter.formatMessage("messages.information.carpet-activation", Map.ofEntries(Map.entry("{CARPET_NAME}", plugin.getCarpet().getName()))));
			plugin.addCarpetUser(new OwnerCarpetDetail(player));
		}
	}

	private void updatedCarpetMileageCounter(final Player player, final MileageCounter mileageCounter, final int currentCounter) {
		plugin.getConnection()
				.map(connection -> new SelectDataTask(connection, player.getUniqueId(), result -> {
					plugin.getServer().getScheduler().runTask(plugin, () -> {
						if (result == null) {
							player.setScoreboard(mileageCounter.showMileageCounter(currentCounter, 0));
						} else {
							player.setScoreboard(mileageCounter.showMileageCounter(currentCounter, result + currentCounter));
						}
					});
				}))
				.ifPresent(task -> plugin.getServer().getScheduler().runTaskAsynchronously(plugin, task));
	}

	private void setUsedCarpet(final PlayerMoveEvent event, final OwnerCarpetDetail ownerCarpetDetail) {
		final List<MetadataValue> blockData = new ArrayList<>();
		blockData.addAll(event.getFrom().getBlock().getLocation().add(0, -1, 0).getBlock().getMetadata(Constant.TYPE_METADATA));
		blockData.addAll(event.getTo().getBlock().getLocation().add(0, -1, 0).getBlock().getMetadata(Constant.TYPE_METADATA));
		blockData.stream()
				.filter(metadataValue -> metadataValue.asString().equals(String.valueOf(ownerCarpetDetail.getPlayer().getUniqueId())))
				.findFirst()
				.ifPresentOrElse(metadataValue -> ownerCarpetDetail.setUsed(true),
						() -> ownerCarpetDetail.setUsed(false));
	}

	private void showCarpetMileageCounter(final PlayerMoveEvent event, final OwnerCarpetDetail ownerCarpetDetail) {
		setUsedCarpet(event, ownerCarpetDetail);
		final MileageCounter mileageCounter = plugin.getMileageCounter();
		int currentCounter = ownerCarpetDetail.getLastMileageCounter();
		if (ownerCarpetDetail.isUsed() && ((event.getTo().getBlockZ() - event.getFrom().getBlockZ() != 0 || event.getTo().getBlockX() - event.getFrom().getBlockX() != 0 || event.getTo().getBlockY() - event.getFrom().getBlockY() != 0))) {
			ownerCarpetDetail.setLastMileageCounter(currentCounter + 1);
		}
		updatedCarpetMileageCounter(event.getPlayer(), mileageCounter, currentCounter);
	}
}
