package com.gmail.kolecka96.spigot.magiccarpetplugin.entity;

import lombok.Data;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@Data
public class OwnerCarpetDetail {
	@NotNull
	private Player player;
	private int lastMileageCounter;
	boolean used;

	public OwnerCarpetDetail(@NotNull final Player player) {
		this.player = player;
	}
}
