package com.gmail.kolecka96.spigot.magiccarpetplugin.entity;

import com.gmail.kolecka96.spigot.magiccarpetplugin.MagicCarpetPlugin;
import com.gmail.kolecka96.spigot.magiccarpetplugin.converter.MessageConverter;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Score;

@Getter
@Setter
public class MileageCounter {
	private final MagicCarpetPlugin plugin;
	private final MessageConverter converter;
	private int lastScore;

	public MileageCounter(final MagicCarpetPlugin plugin, MessageConverter converter) {
		this.plugin = plugin;
		this.converter = converter;
	}

	public Scoreboard showMileageCounter(final int currentCounter, final int total) {
		final ScoreboardManager manager = Bukkit.getScoreboardManager();
		final Scoreboard mileageCounter = manager.getNewScoreboard();
		final Objective registerMileageCounter = mileageCounter.registerNewObjective("Mileage counter", "", "Mileage counter");
		registerMileageCounter.setDisplaySlot(DisplaySlot.SIDEBAR);
		final Score currentMileage = registerMileageCounter.getScore(converter.formatMessage("messages.mileage-counter.current-mileage"));
		currentMileage.setScore(currentCounter);
		final Score totalMileage = registerMileageCounter.getScore(converter.formatMessage("messages.mileage-counter.total-mileage"));
		totalMileage.setScore(total);
		registerMileageCounter.setDisplayName(converter.formatMessage("messages.mileage-counter.header"));
		return mileageCounter;
	}

	public void hideMileageCounter(final Player player) {
		player.setScoreboard(plugin.getServer().getScoreboardManager().getMainScoreboard());
	}
}
