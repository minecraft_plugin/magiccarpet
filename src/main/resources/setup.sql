CREATE TABLE IF NOT EXISTS mileage
  (
     id        INT PRIMARY KEY auto_increment,
     user_id   VARCHAR(64),
     counter   INT,
     last_time DATETIME
  );
